## What kinds of behavior cause sexual harassment?

Any unwelcome verbal, visual or physical conduct sexual behavior is caused by sexual harassment. It might be a verbal comment on any person about their body, clothes, or gender, and also it might be a visual image, text message, or any video that can offend anyone or it can be physical contact like unwanted touch, etc.

### Sexual Harassment is three types:

1. Verbal
2. Visual
3. Physical

### Verbal Harassment

Verbal harassment is like commenting on anyone about their gender, clothes, personal or sexual relationship, Body, etc.

### Visual Harassment

Visual Harassment is like keeping sexual posters, wallpaper, sharing sexual visual content, texting, or emailing sexual content.

### Physical Harassment

Physical Harassment is like unwanted touch, blocking movement, sexual assault, etc.

## What would you do in case you face or witness any incident or repeated incidents of such behavior?

Obtain and become familiar with organization policy on sexual harassment.

Do not take sexual harassment lightly. If you think you are being sexually harassed by an individual or a group, do not accept it as a joke. Do not encourage the harasser by smiling, laughing at his/her jokes, or flirting back. Let the harasser know that you do not enjoy and do not want this type of attention.

At the first time if the harassment belongs to verbal or visual type then tell the harasser that this behavior is not welcomed and ask for the stop that harassment to the harasser.

If this is repetitive then take note of harassment like type, date, time, and what your response is,, and if the harassment is not stopped, write about the incident to ICC in your company.

If I know someone who is being harassed, I will give him or her my support. Encourage the recipient to talk about it and to take immediate action to stop it.
