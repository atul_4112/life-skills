# Listening and Active Communication

## What are the steps/strategies to do Active Listening?

* Don’t start planning what to say next.
* It’s not always easy, but lending a listening, supportive ear can be much more rewarding than telling someone what they should do.
* Take appropriate notes during important meetings.
* Asking relevant questions can show that you’ve been listening and help clarify what has been said.
* Maintain good eye contact - while not staring.
* Make sure that you ask for additional information if you need it, or find an especially interested topic in your conversation to help show your engagement.

## According to Fisher's model, what are the key points of Reflective Listening?

* Eliminating all kinds of distractions.
* Respond to the speaker as much as possible.
* Summarizing the content of the speaker.
* Reflect the mood of the speaker, reflect the emotional state with words.

## What are the obstacles in your listening process?

* I get distracted by my own thoughts if the speaker is speaking continuously.

## What can you do to improve your listening?

* To make a summary of what the speaker says.
* Start making notes.
* Concentrate myself on speaker.

## When do you switch to Passive communication style in your day to day life?

* Whenever I have a conversation with higher authorities.

## When do you switch into Aggressive communication styles in your day to day life?

* When I say something valid and get shunned.
* Something is not happening according to me.
  
## When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

* When I am with my friend, I switch communication to gossiping.

## How can you make your communication assertive? You can analyse the videos and then think what steps you can apply in your own life?

* Choose the right time, one should choose the right time to bring up an issue.



