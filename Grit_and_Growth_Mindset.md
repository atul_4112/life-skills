# Grit and Growth Mindset

## Paraphrase (summarize) the video in a few lines. Use your own words.

- The video talks about the significance of grit and a growth mindset. The speaker talks about the significance of grit above IQ and other parameters.

## What are your key takeaways from the video to take action on?

- To succeed in life iq alone is not enough and grit plays a significant role.

- Having a growth mindset in life can take your performance to the next level.

## Paraphrase (summarize) the video in a few lines in your own words.

- The video talks about the two types of mindsets.
- One being growth and the other being fixed and lists the pros of growth mindset and the cons of fixed mindset.
- Why a growth mindset can help grow your skills and help you improve while having a fixed mindset will stagnate your skills.

## What are your key takeaways from the video to take action on?

- Video talks about the importance of a growth mindset.
- Two types of mindsets are fixed and growth.
- Fixed mindset makes you believe that skills are something you are born with and cannot be improved with effort.
- Growth mindset makes one believe that skills are something that you can get good at by practicing.
- People with a growth mindset focus on learning rather than the outcomes.
  \*People with a growth mindset welcome constructive criticism and use that to learn from their mistakes.

## What is the Internal Locus of Control? What is the key point in the video?

- The video talks about the external locus of control and internal locus of control, and how important it is to have an internal locus of control.
- Internal locus of control is the belief that failures or success is a result of something that is in our control and hence can be influenced by us.

## Paraphrase (summarize) the video in a few lines in your own words.

- The video further talks about the growth mindset, and how the growth mindset helps you take control of your life.
- The video tells you that the inability to do something should be the start of a learning journey that results in the accomplishment of the task and leaves you with a new skill.

## What are your key takeaways from the video to take action on?

- Believe in your ability to figure things out.

- Question your negative assumptions.

- Create your curriculum for skill development.

- Honor the struggle.

## What are one or more points that you want to take action on from the manual? (Maximum 3)

- I am very good friends with Confusion, Discomfort, and Errors.
  Confusion tells me there is more understanding to be achieved.
  Discomfort tells me I have to make an effort to understand. I understand the learning process is uncomfortable.
  Errors tell me what is not working in my code and how to fix it.

- I will follow the steps required to solve problems:
  Relax
  Focus - What is the problem? What do I need to know to solve it?
  Understand - Documentation, Google, Stack Overflow, GitHub Issues, Internet
  Code
  Repeat
- I will not leave my code unfinished till I complete the following checklist:
  Make it work.
  Make it readable.
  Make it modular.
  Make it efficient.
- I know more efforts lead to better understanding.
