# Learning Process

## What is the Feynman Technique?

Feynman's learing Technique is if you understand stuff express it on simple word. Feynman provided several essential details that clarified how this idea was put into practice.

- The concept's name must be written at the top of a piece of paper.
- Try to explain the concept in simple language.
- Identify the problem areas and then go back to the sources to review.
- Review your explanation and try to eliminate any convoluted or technical terms and make it simple terms.

## What are the different ways to implement this technique in your learning process?

- By keep learning new things.
- Learning in multiple ways.
- Teaching to other what I am learning.
- Build the strategy on previous learning.
- Gaining practical experience.
- Taking test by regulary what I am larn till.

## Paraphrase the video in detail in your own words.

From this video we can know how learning process can make efficient.

- Think about a problem with your active focus but also take breaks in between so that your passive brain can think of new ways to solve the said issue.
- Try to play to the strengths; as examples, if it take longer to understand information, don't see this as a weakness; rather, realize that you are understanding what you are learning much more deeply.

## What are some of the steps that you can take to improve your learning process?

- Try to teach the topic that you are trying to learn in simple and non convoluted language.
- Identify the problem areas and then go back to the sources to review.
- While learning something new, take breaks in between, this lets the brain passively think on the topic and create neural pathways.

## Your key takeaways from the video? Paraphrase your understanding.

The video talks about how one should approach learning a new skill. The video emphasizes the point that one actually doesn't need hundreds of hours to learn a new skill and just needs 20 hours to learn enough that one can work with the skill.

## What are some of the steps that you can while approaching a new topic?

Some of the steps that can be implemented while adopting a new skill are:-

- Learn enough to self-correct, learn enough about something that you can correct yourself, and know the correct resources to learn.
- Remove practice barriers, that is do not judge yourself while learning a new skill if you get underconfident, it becomes harder to learn something.
- Deconstruct the skill, meaning learn about the different parts of the skill and prioritise the important topics first.
- Learn something for at least for 20 hours.
