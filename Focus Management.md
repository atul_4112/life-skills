# Focus Management

## What is Deep Work?

- Working without getting distractions and improving your productivity.

## Paraphrase all the ideas in the above videos and this one in detail.

- The first video talks about deep work, and how long you should work without taking a break, the video describes that the minimum duration of deep work should be 1 hour.
- The second video talks about deadlines and their effectiveness.
- Deadlines work because of several reasons some of which are :-
  - Deadlines serve as a motivation to work, as one looks to finish the work before the deadline.
  - Deadlines help to regulate breaks as one knows how long they need to work before taking a break.
- Third video talks about the fact that hard work leads to the formation of myelin, which increases brain function.
- The video also talks about some strategies to implement deep work which are:
  - Schedule distractions or breaks.
  - Make deep work a habit or a sinusoidal function of time.
  - Get adequate sleep.

## How can you implement the principles in your day to day life?

- Some steps that implement deep work in your day-to-day life are :-
  - At least practice deep work for 1 hour a day.
  - Schedule distractions or breaks.
  - Practice deep work regularly and make it a habit.
  - Get adequate sleep.

## Your key takeaways from the video.

- Some key points of the video :
  - When Social Media wastes too much time it is not a healthy thing.
  - Social Media keeps distracting you as it is designed to harness your attention.
  - Use of social media is also bad for one's mental health as it leads to a comparison of lives which can end up saddening a person.
