# Energy Management

## What are the activities you do that make you relax - Calm quadrant?

Some of the activities that calm me down are:-

- Talking to my friends.
- Watching comedy movies.
- Playing games.

## When do you find getting into the Stress quadrant?

I find myself getting into the stress quadrant when I have a lot of work and not enough time to finish it.

## How do you understand if you are in the Excitement quadrant?

When I am feeling like something is about to give or something good is going to happen.

## Paraphrase the Sleep is your Superpower video in detail.

The video explains that lack of sleep has a devastating effect on your body, lack of sleep not only increases the risk of depression but also increases all sorts of diseases.

- Sleep deprivation can reduce your learning capacity by 40%.
- It was found that sleep deprivation also causes the DNA to increase the production of cancer-causing cells.
- A study showed that lack of sleep also negatively affects your immune system.
- Sleep deprivation also causes your hippocampus to stop receiving and sending signals.

## What are some ideas that you can implement to sleep better?

Some of the things one can implement to get better sleep are:-

- Sleep at a regular time.
- Maintain a temperature of 18 degrees Celsius in the room you are planning to sleep.

## Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points.

The video explains that a lack of exercise negatively changes your brain structure and increases the risk of cognitive diseases.

- Exercising can increase the volume of the hippocampus.
- Exercising also increases memory function.
- Exercising can increase focus span.
- Exercising makes your pre-frontal cortex more immune to cognitive diseases.
- Exercising can reduce the risk of diseases.

## What are some steps you can take to exercise more?

Some of the steps you can take are:-

- Use of stairs.
- Taking short exercise breaks.
- Walking whenever possible.
