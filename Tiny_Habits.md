# Tiny Habits

## Your takeaways from the video (Minimum 5 points).

In the video, which explains how to create tiny habits, the speaker outlines certain essential guidelines to follow :

- Instead of seeking out inspiration to form a habit, one should start small and work their way up.
- The video outlines the three components of behavior that are (MAP).
- 'M' refers to Motivation.
- 'A' refers to Ability.
- 'P' refers to the Prompt, which is the cue to do something.
- According to the video, tiny habits work because:
  - You may get started right away because they are easy to complete and adapt to any schedule.
  - They are risk-free compared to huge or risky actions.
  - It's simple to attempt again if you make a mistake.
- In the final segment of the film, it is discussed how to combine a tiny habit with an existing one so that the latter acts as a cue for the former.

## Your takeaways from the video in as much detail as possible.

- The video further breaks down the points discussed in the previous video.
- The video explains how BJ gave the (B=MAP) behavior model, in which he highlighted that behavior change is attainable not only through motivation but can also be accomplished by constructing or breaking down a change to its most minute action and making that a habit.
- In the following instances, he clarified why this approach is effective:
  - You may get started right away because they are easy to complete and adapt to any schedule.
  - They are risk-free compared to huge or risky actions.
  - It's simple to attempt again if you make a mistake.
- He further goes on to explain that one needs an action prompt to successfully create a new habit, which means pairing a habit with a pre-existing habit so that the momentum from the previous action helps you do the little task that will form a new habit.

## How can you use B = MAP to make making new habits easier?

The following actions can be taken to make a new habit easier:

- Break the change into little tiny habits.
- Pair the habits with an action prompt.
- Celebrate after small victories.

## Why it is important to "Shine" or Celebrate after each successful completion of habit?

After finishing a habit, it's crucial to rejoice since it gives you the feeling that you've accomplished something and, in turn, serves as a psychological reward for the behavior.

## Your takeaways from the video (Minimum 5 points).

The video discusses developing habits. The process is broken down into a few steps, some of which include Noticing Wanting Doing Liking. Some of the points shared in the video are:

- Your habits accumulate throughout time, whether they are good or bad, and time is a key factor in this process.
- Set a specific time and place for your goals and set a fixed time to execute said tasks.
- Make a backup plan for your habits so that in case you miss one, you still have a chance to follow it.
- Design your environment to support your habit.
- Place steps between you and a behavioral pattern you want to avoid.
- Make a plan to improve the beginning, not the end.

## Write about the book's perspective on habit formation from the lens of Identity, processes and outcomes?

The book tells you to change your identity which will eventually lead to a change in habit formation. The processes discussed are:

- Instead of focusing on the result, make an effort to create a sustainable trip or a plan that will enable you to keep playing the game.
- The result of every procedure described in the book will be the development of a habit and mastery of it.

## Write about the book's perspective on how to make a good habit easier?

- The book begins with the idea that habits compound with time, and that time is both an ally and an enemy depending on whether a habit is good or bad.
- The book discusses the plateau of disappointment which is the disappointment one feels when he feels the progress planned is not being achieved, which is a normal thing as progress does not have a linear relationship with time. Then the book brings up the point of sustainability, as to plan the journey or build the system to continue playing the game, as setting goals can be pointless if the goals are too hard to achieve one would lose the motivation, or if the goal is reached one would stop trying.
- The concept that identity change is the path to habit modification is then brought up in the text. The book concludes with a number of suggestions for how to develop a new habit that will last, including some of the following:
  - Break the change into little tiny habits.
  - Pair the habits with an action prompt.
  - Celebrate after small victories.

## Write about the book's perspective on making a bad habit more difficult?

The book suggests adding steps or increasing the distance between you and the habit you want to change or give up.

## Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

- Every day, I want to make it a habit to answer at least one Hackerrank interview question.
- I can pair this habit with my work as an activity that I would end the day with.
- The end of work will serve as a cue to the habit. I can treat myself with candy as a reward for achieving my goal.

## Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?

Overthinking is a habit I'd like to break, and I can do this by applying certain ideas that tell me it's useless to do so.
