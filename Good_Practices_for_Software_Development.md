# Good Practices for Software Development

## What is your one major takeaway from each one of the 6 sections. So 6 points in total.

- Sometimes despite all efforts, the requirements are still vague. So during the implementation phase, get frequent feedback so that you are on track and everyone is on the same page

- Do not miss calls. If you cannot talk immediately, receive the call and inform them you will call in back in 5-10 min, and call them back. This is a much better alternative than letting it go down as a missed call.
- Look at the way issues get reported in large open-source projects
- Join the meetings 5-10 mins early to get some time with your team members
- Remember they have their own work to do as well. Pick and choose your communication medium depending on the situation
- Always keep your phone on silent. Remove notifications from the home screen. Only keep notifications for work-related apps

## Which area do you think you need to improve on? What are your ideas to make progress in that area?

- I have to improve myself in React concepts in that especially i have to focus on class components.
- Practice regularly or be in touch with previously learned technical concepts.
- for improving react I will do on-hand practice whenever I learn concepts and read articles related that concepts.
- Be updated regularly on the internet related to concepts.
